from tkinter import Tk, Grid

from gui import Playground


def main():
    root = Tk()
    Grid.rowconfigure(root, 0, weight=1)
    Grid.columnconfigure(root, 0, weight=1)

    Playground(root, 5, 5, 2)
    root.mainloop()


if __name__ == '__main__':
    main()
