from random import randrange
import copy

def pprint(matrix):
    for row in matrix:
        for elem in row:
            if elem == -1:
                print('x', end=' ')
            else:
                print(elem, end=' ')
        print()


class Minesweeper:
    def __init__(self, rows, columns, mines):
        self.matrix = [[0 for _ in range(columns)] for _ in range(rows)]
        self.rows = rows
        self.columns = columns
        self.mines = mines

        self._add_mines(mines)
        self._add_numbers()

    def _add_mines(self, mines):
        for _ in range(mines):
            x, y = randrange(self.rows), randrange(self.columns)
            while self.matrix[x][y] == -1:
                x, y = randrange(self.rows), randrange(self.columns)
            self.matrix[x][y] = -1

    def _inside_matrix(self, x, y):
        return 0 <= x < self.rows and 0 <= y < self.columns

    def _number_of_neighbour_mines(self, x, y):
        pozs = [(-1, -1), (-1, 0), (-1, 1), (0, 1), (1, 1), (1, 0), (1, -1), (0, -1)]
        neighs = [(row + x, col + y) for row, col in pozs if self._inside_matrix(row + x, col + y)]
        counter = sum(1 for row, col in neighs if self.matrix[row][col] == -1)
        return counter

    def _add_numbers(self):
        for i in range(self.rows):
            for j in range(self.columns):
                if self.matrix[i][j] != -1:
                    self.matrix[i][j] = self._number_of_neighbour_mines(i, j)

    def __fill(self, x, y, colect):
        if self._inside_matrix(x, y) and self.matrix[x][y] == 0:
            self.matrix[x][y] = -10
            self.__fill(x + 1, y, colect)
            self.__fill(x, y + 1, colect)
            self.__fill(x - 1, y, colect)
            self.__fill(x, y - 1, colect)
            colect.add((x, y))

    def get_0_neighs(self, row, col):
        pozs = [(-1, -1), (-1, 0), (-1, 1), (0, 1), (1, 1), (1, 0), (1, -1), (0, -1)]
        rez = set()

        mat = copy.deepcopy(self.matrix)
        self.__fill(row, col, rez)
        self.matrix = mat

        rez_all = set()
        for i, j in rez:
            neighs = [(row + i, col + j) for row, col in pozs if self._inside_matrix(row + i, col + j)]
            rez_aux = {pair for pair in neighs}
            rez_all |= rez_aux

        return rez | rez_all
