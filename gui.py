from functools import partial

from tkinter import ttk, DISABLED, messagebox, NORMAL, N, S, E, W, Grid

from minesweeper import Minesweeper


class Playground:
    def __init__(self, master, rows, cols, mines):
        self.frame = ttk.Frame(master)
        self.frame.grid(row=0, column=0, sticky=N+S+E+W)

        self.pressed = 0
        self.win = rows * cols - mines

        self.btns = [[ttk.Button for _ in range(cols)] for _ in range(rows)]
        self.rows = rows
        self.cols = cols
        self.mines = mines

        self.game = Minesweeper(rows, cols, mines)

        for i in range(rows):
            Grid.rowconfigure(self.frame, i, weight=1)
            for j in range(cols):
                Grid.columnconfigure(self.frame, j, weight=1)

                btn = ttk.Button(self.frame, text="", width=5, command=partial(self.show, i, j))
                btn.bind("<Button-3>", partial(self.put_flag, btn))
                btn.grid(row=i, column=j, sticky=N+S+E+W)
                self.btns[i][j] = btn

    @staticmethod
    def put_flag(btn, event):
        if str(btn['state']) != 'disabled':
            btn.config(text="") if btn['text'] == 'F' else btn.config(text="F")

    def _new_game(self):
        self.game = Minesweeper(self.rows, self.cols, self.mines)
        self.pressed = 0
        for i in range(self.rows):
            for j in range(self.cols):
                self.btns[i][j].config(text="", state=NORMAL)

    def show(self, row, col):
        number = self.game.matrix[row][col]
        if number == -1:
            messagebox.showwarning("Ai pierdut", "Ai apasat pe o bomba !")
            self._new_game()
        elif number == 0:
            all = self.game.get_0_neighs(row, col)
            self.pressed += len(all)
            for i, j in all:
                self.btns[i][j].config(text=self.game.matrix[i][j], state=DISABLED)
        else:
            self.btns[row][col].config(text=number, state=DISABLED)
            self.pressed += 1

        if self.pressed - 1 == self.win or self.pressed == self.win:
            messagebox.showinfo("Felicitari !", "Ai castigat ^^ ")
            self._new_game()
